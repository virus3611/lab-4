﻿using System;

namespace lab_4._2
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Console.Write("Enter number: ");
            int number = int.Parse(Console.ReadLine());
            long timeDoFunction = getTimeMilliseconds();
            for (int i = 0; i < 1000000; i++) Cut(number);
            long timeEndFunction = getTimeMilliseconds();
            Console.WriteLine("Время работы функции: {0} млс", timeEndFunction - timeDoFunction);
            Console.WriteLine("number: {0}", Cut(number));
        }

        public static int Cut(int number)
        {
            int y = (int)Math.Pow(10, (int)Math.Log10(number));
 
            number = (number - number / y * y) / 10;
            return number;
        }

        static long getTimeMilliseconds()
        {
            DateTime time = DateTime.Now;
            return (((time.Hour * 60 + time.Minute) * 60 + time.Second) * 1000
                    + time.Millisecond);
        }
        
    }
}