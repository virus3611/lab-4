﻿using System;

namespace Lab_4._4
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Введите а: ");
            double aVeriable = int.Parse(Console.ReadLine());
            Console.Write("Введите степень: ");
            int power = int.Parse(Console.ReadLine());
            Console.WriteLine("aVeriable^(-n) = {0}",function(aVeriable, power));
        }

        static double function(double aVeriable, long n)
        {
            if (n <= 0) 
                return 1;
            else
                return 1 / aVeriable * function(aVeriable, n - 1);   
        }
    }
}