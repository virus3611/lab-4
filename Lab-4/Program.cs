﻿using System;

namespace Lab_4
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Console.Write("Enter number n = ");
            int n = int.Parse(Console.ReadLine());
            Console.WriteLine("Сумма = {0}", Factorial_Count(n));
        }

        public static int Factorial_Count(int n)
        {
            int result = 0;
            for (int i = 1; i <= n; i++)
            {
                result += i;
            }
            return result;
        }
    }
}