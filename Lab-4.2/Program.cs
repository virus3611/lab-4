﻿using System;

namespace lab_4._1
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            int number = 1;
            int uneven = 0;    //нечетное колличество чисел
            while (number != 0)
            {
                Console.Write("Enter number: ");
                number = int.Parse(Console.ReadLine());
                if(number % 2 != 0)
                    uneven += 1;
            }
            Console.WriteLine("Volume of uneven numbers: {0}", uneven);
        }
    }
}